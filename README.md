# Dockerized-Emacs
## Overview
This is a dockerized implementation of Emacs26, coupled with my .emacs.d configuration. [Based on JAremko's docker-emacs container.](https://github.com/JAremko/docker-emacs/)
## Getting Started
### Pull Pre-built Image
This pre-built image is expected to run out of the box on systems whose primary user has a UID of 1000 and a GID of 1000. This is the most likely situation, thus why the pre-built image uses these expectations.
> * docker pull bradthebuilder/emacs:TAG

### Build For Your System
Because docker containers share the host's kernel, and thus UIDs and GIDs, the Dockerfile expects build arguments to specify the host's UID and GID that the container is expecting to run under. The host's UID and GID are required so that mounting the user's project directory results in proper ownership and thus access to those files so that they can be worked on from within the container.

> * `git clone https://bradthebuilder@bitbucket.org/bradthebuilder/emacs.git`
* `cd ./emacs`
* `docker build --build-arg DOCKER_GROUP_ID=$(getent group docker | cut -d: -f3) --build-arg HOST_UID=$(id -u) --build-arg HOST_GID=$(id -g) .`

The image also assumes a docker GID of 998. If it is something different, you will also have to build the image for your system.

### Running The Container
`docker run --rm -d --name myemacs -v /tmp/.X11-unix:/tmp/.X11-unix:ro\
 -e DISPLAY="unix$DISPLAY"\
 -e UNAME="emacs"\
 -e GNAME="emacs"\
 -e UID=$(id -u)\
 -e GID=$(id -g)\
 -v ~/projects:/mnt/workspace\
 -v /var/run/docker.sock:/var/run/docker.sock\
 -v ~/.ssh:/home/emacs/.ssh\
 bradthebuilder/emacs:TAG emacs`

Consult the [maintainer's documentation for more information](https://github.com/JAremko/docker-emacs/) on running the container.

## Implementation
This is a monolithic image that contains the system capabilities used from within Emacs (programming languages, binaries, etc). It is consequently very large and deliberately violates the [Docker recommended best practice](https://docs.docker.com/develop/dev-best-practices/#how-to-keep-your-images-small) of keeping images small for the desired tradeoff of maximum portability.

Integrated Components:

* .emacs.d config
* Various binaries
* Various system packages

Mounted Components:

* Host user's ~/.ssh to enable git from within container
* Host's docker.sock file to control host-level docker daemon from within container
* Host's ~/project folder to edit files from within container

A future implementation will remove integrated packages and binaries and leverage more volume mounts to make OS packages and binaries accessible from within the container. It is also possible to decouple the emacs config from the container, but it requires yet another mount and the manual task of cloning/ensuring the [config repsoitory](https://bitbucket.org/bradthebuilder/emacs.d/src/develop/) is on the target system at ~/.emacs.d
