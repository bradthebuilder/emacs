FROM jare/emacs:latest
ARG DOCKER_GROUP_ID
ARG HOST_UID
ARG HOST_GID
ENV UNAME="emacs"
COPY .emacs.d ${UHOME}/.emacs.d
RUN mkdir -p ${UHOME}/.local/share/fonts && mkdir ${UHOME}/.emacs.d/lisp \
&& cd ${UHOME}/.emacs.d/lisp && git clone https://github.com/emacsmirror/esv.git
COPY .zshrc ${UHOME}/.zshrc
COPY ./source-code-pro/TTF/ ${UHOME}/.local/share/fonts
COPY ./source-sans-pro/TTF/ ${UHOME}/.local/share/fonts
COPY requirements.txt ${UHOME}/requirements.txt
COPY packages.txt ${UHOME}/packages.txt
COPY gpg ${UHOME}/gpg
COPY .gitconfig ${UHOME}/.gitconfig
COPY docker.list /etc/apt/sources.list.d/docker.list
RUN apt-key add ${UHOME}/gpg \
    && groupadd -g ${DOCKER_GROUP_ID} docker && groupadd -g ${HOST_GID} emacs \
    && apt-get update && xargs -a ${UHOME}/packages.txt apt-get install -y \
    && useradd -G docker -ms /usr/bin/zsh -u ${HOST_UID} -g ${HOST_GID} ${UNAME} \
    && sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)" \
    && mv /root/.oh-my-zsh ${UHOME}/ \
    && pip3 install -r ${UHOME}/requirements.txt \
    && wget https://releases.hashicorp.com/terraform/0.12.20/terraform_0.12.20_linux_amd64.zip -O \
    ${UHOME}/tf.zip && unzip ${UHOME}/tf.zip -d /usr/local/bin && rm ${UHOME}/tf.zip \
    && wget https://github.com/terraform-linters/tflint/releases/download/v0.14.0/tflint_linux_amd64.zip \
    -O ${UHOME}/tfl.zip && unzip ${UHOME}/tfl.zip -d /usr/local/bin && rm ${UHOME}/tfl.zip \
    && wget https://dl.google.com/go/go1.13.8.linux-amd64.tar.gz -O ${UHOME}/go.tar.gz && \
    tar -C /usr/local -xzf ${UHOME}/go.tar.gz && rm ${UHOME}/go.tar.gz && \
    wget https://github.com/hadolint/hadolint/releases/download/v1.17.5/hadolint-Linux-x86_64 -O \
    /usr/local/bin/hadolint && chmod +x /usr/local/bin/hadolint
# Create ${UNAME} user and install Emacs packages
RUN UID=${HOST_UID} GID=${HOST_GID} asEnvUser emacs -nw -batch -u ${UNAME} -q -kill
