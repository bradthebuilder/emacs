#!/bin/sh
RAND_UID=$(shuf -i 60000-65000 -n 1)
RAND_GID=$(shuf -i 60000-65000 -n 1)
# Add groups and users
groupadd -g $RAND_GID emacs
useradd -G docker -m -u $RAND_UID -g $RAND_GID emacs
echo "Created $RAND_UID:$RAND_GID"
